<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
        [
            'id' => '1',       
            'name' => 'user1',
            'email' => 'a@a.com',
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            'role' => 'manager',
        ],

        [
            'id' => '2',       
            'name' => 'user2',
            'email' => 'b@b.com',
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            'role' => 'salesrep',
        ],

        [
            'id' => '3',       
            'name' => 'user3',
            'email' => 'c@c.com',
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            'role' => 'salesrep',
        ],
            ]);
    }
}
