
@yield('content')

@extends('layouts.app')


@section('content')


<h1>Add a Customer</h1>
<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

<form method = 'post' action = "{{action('CustomersController@store')}}" >

{{csrf_field()}}


<div class = "form-group">
<label for = "name" > Name </label>
<br>
<input type = "text" class = "form-control" name = "name">
<br>
<p>E-mail:</p>
<input type = "text" class = "form-control" name = "email">

<p>Phone:</p>
<input type = "text" class = "form-control" name = "phone">
</div>



<div class = "form-group">
<input type = "submit" class = "form-control" name = "submit" value = "Save">
</div>





</form>
@endsection